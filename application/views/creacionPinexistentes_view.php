<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reparacion peso silos</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg  navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Tecnoandina</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="http://localhost/influx/index.php/Welcome/index">Correcion puntos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="http://localhost/influx/index.php/Welcome/union">Union de puntos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="http://localhost/influx/index.php/Welcome/creacionP">Cambio putnos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="http://localhost/influx/index.php/Welcome/creacionPi">creacion putnos inexistentes</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container" >
    <br>


    <div class="row">
        <div class="col-6">
            <h1 class="text-center">Creacion putnos inexistentes</h1>
        </div>
        <div class="col-6">
            <select name="sector" id="sector" class="form-control">
                <option value="bosque">Bosque Viejo</option>
                <option value="esmeralda">Esmeralda</option>
                <option value="mirador">Mirador</option>
            </select>
        </div>
    </div>
    <br>
    <form action="#">
        <div class="row">
            <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                <div class="row">
                    <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                        <label for="pabellon">Numero de pabellon (1-32)</label>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                        <input type="number" class="form-control" placeholder="pabellon" name="pabellon" id="pabellon" >
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                <div class="row">
                    <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                        <label for="kilos">kilos inicio s1 </label>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                        <input type="number" placeholder="kilos" class="form-control" id="kilosIS1">
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                <div class="row">
                    <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                        <label for="kilos">kilos termino s1 </label>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                        <input type="number" placeholder="kilos" class="form-control" id="kilosTS1">
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                <div class="row">
                    <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                        <label for="kilos">kilos inicio s2 </label>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                        <input type="number" placeholder="kilos" class="form-control" id="kilosIS2">
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                <div class="row">
                    <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                        <label for="kilos">kilos termino s2 </label>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                        <input type="number" placeholder="kilos" class="form-control" id="kilosTS2">
                    </div>
                </div>
            </div>


        </div>
        <br>
        <div class="row">
            <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                <div class="row">
                    <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                        <label for="Finicio">Fecha de inicio</label>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                        <input type="datetime-local" class="form-control" placeholder="Fecha inicio" name="Finicio" id="Finicio">
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                <div class="row">
                    <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                        <label for="Ftermino">Fecha de termino</label>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                        <input type="datetime-local" class="form-control" placeholder="Fecha termino" name="Ftermino" id="Ftermino">
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-6">
                <div class="row">
                    <button type="button" class="btn btn-success" style="margin: auto" id="modificar">Modificar</button>
                </div>
            </div>

        </div
    </form>
    <br>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script >
    $("#modificar").click(function () {
        // alert("modificar");
        var pabellon= document.getElementById("pabellon").value;

        var kilos_inicio_s1=document.getElementById("kilosIS1").value;
        var kilos_termino_s1=document.getElementById("kilosTS1").value;
        var kilos_inicio_s2=document.getElementById("kilosIS2").value;
        var kilos_termino_s2=document.getElementById("kilosTS2").value;

        var fecha_inicio= document.getElementById("Finicio").value;
        var fecha_termino=document.getElementById("Ftermino").value;

        var date_1 = new Date(fecha_inicio.split("T")[0]+" "+fecha_inicio.split("T")[1]+":00");
        var date_2 = new Date(fecha_termino.split("T")[0]+" "+fecha_termino.split("T")[1]+":00");

        var day_as_milliseconds = 86400000;
        var diff_in_millisenconds = date_2 - date_1;
        var diff_in_days = diff_in_millisenconds / day_as_milliseconds;


        var sector=document.getElementById("sector").value;  

        var urlD;

        switch (sector){
            case "bosque":urlD="http://52.234.128.253:1880/creacionPtosInexistentes"
                break;
            case "esmeralda":urlD="http://52.234.128.253:1880/creacionPtosInexistentesEsmeralda"
                break;
            case "mirador":urlD="http://52.234.128.253:1880/creacionPtosInexistentesMirador"
                break;
        }

        alert(urlD);

        if(diff_in_days>=0 ){
            $.ajax({

                data: {"pabellon" :  pabellon,"kilos_inicio_s1":kilos_inicio_s1,"kilos_termino_s1":kilos_termino_s1, "kilos_inicio_s2":kilos_inicio_s2,"kilos_termino_s2":kilos_termino_s2,"fecha_inicio":fecha_inicio.split("T")[0]+" "+fecha_inicio.split("T")[1]+":00","fecha_termino":fecha_termino.split("T")[0]+" "+fecha_termino.split("T")[1]+":00" },

                type: "post",

                dataType: "json",

                url:urlD,

            })
                .done(function( result, textStatus, jqXHR ) {
                    alert("operación realizada");
                })
                .fail(function( jqXHR, textStatus, errorThrown ) {
                    // alert("error");
                });


        }else{
            alert("no se puede buscar,exele el limite o la fehca de termino es inferior a la de inicio");
        }



    });


</script>
</body>
</html>