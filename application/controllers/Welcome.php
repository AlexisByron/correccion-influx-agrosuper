<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {


	public function index()
	{
		$this->load->view('correccion_view');
	}

    public function union()
    {
        $this->load->view('union_view');
    }

    public function creacionP(){
	    $this->load->view('creacionPtos_view');
    }

    public function creacionPi(){
        $this->load->view('creacionPinexistentes_view');
    }

}
